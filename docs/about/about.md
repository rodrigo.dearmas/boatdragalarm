# Sobre mi

![](../images/per.jpg){ width=30% }

Mi nombre es Rodrigo Dearmas, estudié Ingeniería de Sistemas y desde el 2009 me dedico a la robótica educativa. Comencé trabajando en el laboratorio de robótica (MINA) de Faculdad de Ingeniería de UdelaR, ayudando en el desarrollo del robot Butiá y luego en 2014 fundé UYROBOT junto a mi socio John Pereira.
Estos años en UYROBOT hemos desarrollado distintos programas educativos y material para poder hacer accesible la robótica a la mayor cantidad de niños posible.

En 2020 fundé maker.uy plataforma dedicada principalmente a la Fabricación Digital, Realizamos cortes laser e impresión 3d como principal producto.

##Links de interés

[UYROBOT](http://uyrobot.com/)

[maker.uy](https://www.instagram.com/maker.uy/)

[Butiá](https://www.fing.edu.uy/inco/proyectos/butia/)

[MINA](https://www.fing.edu.uy/inco/grupos/mina/)

[Youtube](https://www.youtube.com/channel/UCrEUDZYDJQ5VYN-yHv6WaNQ)


## Áreas de expertise
![](../images/habrodrigo.png)
![](../images/hab1rodrigo.png)
